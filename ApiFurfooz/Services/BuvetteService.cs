﻿using ApiFurfooz.DAL.Entities;
using ApiFurfooz.DAL.Repositories;
using ApiFurfooz.Filters;
using ApiFurfooz.Mapper;
using ApiFurfooz.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Drawing.Text;
using System.Linq;
using System.Runtime.Remoting.Messaging;
using System.Web;

namespace ApiFurfooz.Services
{
    public class BuvetteService
    {
        private BuvetteRepository repo;
        public BuvetteService(BuvetteRepository repo)
        {
            this.repo = repo;
        }

        public IEnumerable<BuvetteModel> GetAll()
        {
            try
            {
                return repo.Get().Select(x=>x.MapTo<BuvetteModel>());
            }
            catch(Exception e)
            {
                throw new Exception(e.Message);
            }
        }

        public Buvette GetById(int id)
        {
            try
            {
                return repo.GetById(id);
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }
        }


        public int Insert(Buvette a)
        {
            try
            {
                return repo.Insert(a);
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }
        }

        public void Update(Buvette a)
        {
            try
            {
                repo.Update(a);
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }
        }

        public void Delete(int id)
        {
            try
            {
                repo.Delete(id);
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }
        }

    }
}