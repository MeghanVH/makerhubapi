﻿using ApiFurfooz.DAL.Entities;
using ApiFurfooz.DAL.Repositories;
using ApiFurfooz.Mapper;
using ApiFurfooz.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace ApiFurfooz.Services
{
    public class TarifService
    {
        private TarifRepository repo;
        public TarifService(TarifRepository repo)
        {
            this.repo = repo;
        }

        public IEnumerable<TarifModel> GetAll()
        {
            try
            {
                return repo.Get().Select(x => x.MapTo<TarifModel>());
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }
        }

        public Tarif GetById(int id)
        {
            try
            {
                return repo.GetById(id);
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }
        }

        public int Insert(Tarif a)
        {
            try
            {
                return repo.Insert(a);
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }
        }

        public void Update(Tarif a)
        {
            try
            {
                repo.Update(a);
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }
        }

        public void Delete(int id)
        {
            try
            {
                repo.Delete(id);
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }
        }
    }
}