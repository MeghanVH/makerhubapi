﻿using ApiFurfooz.DAL.Entities;
using ApiFurfooz.DAL.Repositories;
using ApiFurfooz.Mapper;
using ApiFurfooz.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace ApiFurfooz.Services
{
    public class PointInteretService
    {

        private PointInteretRepository repo;
        public PointInteretService(PointInteretRepository repo)
        {
            this.repo = repo;
        }

        public IEnumerable<PointInteretModel> GetAll()
        {
            try
            {
                return repo.Get().Select(x => x.MapTo<PointInteretModel>());
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }
        }

        public PointInteret GetById(int id)
        {
            try
            {
                PointInteret pi = repo.GetById(id);
                return pi;
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }
        }

        public int Insert(PointInteret a)
        {
            try
            {
                return repo.Insert(a);
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }
        }

        public void Update(PointInteret a)
        {
            try
            {
                repo.Update(a);
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }
        }

        public void Delete(int id)
        {
            try
            {
                repo.Delete(id);
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }
        }
    }
}