﻿using ApiFurfooz.DAL.Entities;
using ApiFurfooz.DAL.Repositories;
using ApiFurfooz.Mapper;
using ApiFurfooz.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace ApiFurfooz.Services
{
    public class InfoFurfoozService
    {
        private InfoFurfoozRepository repo;
        public InfoFurfoozService(InfoFurfoozRepository repo)
        {
            this.repo = repo;
        }


        public IEnumerable<InfoFurfoozModel> GetAll()
        {
            try
            {
                return repo.Get().Select(x => x.MapTo<InfoFurfoozModel>());
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }
        }

        public InfoFurfooz GetById(int id)
        {
            try
            {
                return repo.GetById(id);
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }
        }

        public int Insert(InfoFurfooz a)
        {
            try
            {
                return repo.Insert(a);
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }
        }

        public void Update(InfoFurfooz a)
        {
            try
            {
                repo.Update(a);
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }
        }

        public void Delete(int id)
        {
            try
            {
                repo.Delete(id);
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }
        }

    }
}