﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Web;
using System.Web.Http;
using System.Web.Http.Controllers;

namespace ApiFurfooz.Filters
{
    public class ApiAuthorization : AuthorizeAttribute
    {
        public ApiAuthorization()
        {
            
        }
        public override void OnAuthorization(HttpActionContext actionContext)
        {
            actionContext.Request.Headers.TryGetValues("Authorization", out IEnumerable<string> tokens);
            if(tokens == null)
            {
                throw new HttpResponseException(System.Net.HttpStatusCode.Unauthorized);
            }
        }
    }
}