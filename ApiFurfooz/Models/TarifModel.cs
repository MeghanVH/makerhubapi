﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace ApiFurfooz.Models
{
    public class TarifModel
    {
        [Required]
        [Range(0,120)]
        public int Age { get; set; }

        [Required]
        [Range(0,999)]
        public double Price { get; set; }
        public bool IsDeleted { get; set; }
    }
}