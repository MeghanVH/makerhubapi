﻿using ApiFurfooz.DAL.Entities;
using ApiFurfooz.Models;
using ApiFurfooz.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using HttpDeleteAttribute = System.Web.Http.HttpDeleteAttribute;
using HttpGetAttribute = System.Web.Http.HttpGetAttribute;
using HttpPostAttribute = System.Web.Http.HttpPostAttribute;
using HttpPutAttribute = System.Web.Http.HttpPutAttribute;

namespace ApiFurfooz.Controllers
{
    public class TarifController : ApiController
    {

        private TarifService service;
        public TarifController(TarifService service)
        {
            this.service = service;
        }

        [HttpGet]
        public IEnumerable<TarifModel> Get()
        {
            try
            {
                IEnumerable<TarifModel> tarifs = service.GetAll();
                return tarifs;
            }
            catch (Exception e)
            {
                throw new HttpResponseException(HttpStatusCode.InternalServerError);
            }
        }

        [HttpGet]

        public Tarif GetById(int id)
        {
            try
            {
                var tarif = service.GetById(id);
                if (tarif == null)
                {
                    return null;
                }
                else
                {
                    return tarif;
                }
            }
            catch (Exception e)
            {
                throw new HttpResponseException(HttpStatusCode.InternalServerError);
            }
        }


        [HttpPut]
        public void Update(Tarif a)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    service.Update(a);
                }
                catch (Exception e)
                {
                    throw new HttpResponseException(HttpStatusCode.InternalServerError);
                }
            }
            else
            {
                throw new HttpResponseException(HttpStatusCode.BadRequest);
            }
        }


        [HttpDelete]
        public void Delete(int id)
        {
            try
            {
                service.Delete(id);
            }
            catch (Exception e)
            {
                throw new HttpResponseException(HttpStatusCode.InternalServerError);
            }
        }


        [HttpPost]
        public int Insert(Tarif a)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    return service.Insert(a);
                }
                catch (Exception e)
                {
                    throw new HttpResponseException(HttpStatusCode.InternalServerError);
                }
            }
            else
            {
                throw new HttpResponseException(HttpStatusCode.BadRequest);
            }
        }
    }
}