﻿using ApiFurfooz.DAL.Entities;
using ApiFurfooz.Models;
using ApiFurfooz.Services;
using System;
using System.Collections.Generic;
using System.Net;
using System.Web.Http;

namespace ApiFurfooz.Controllers
{
    public class CategoryProductController : ApiController
    {

        private CategoryProductService service;

        public CategoryProductController(CategoryProductService service)
        {
            this.service = service;
        }

        [HttpGet]
        public IEnumerable<CategoryProductModel> Get()
        {
            try
            {
                IEnumerable<CategoryProductModel> Categories = service.GetAll();
                return Categories;
            }
            catch (Exception e)
            {
                throw new HttpResponseException(HttpStatusCode.InternalServerError);
            }
        }


        [HttpGet]

        public CategoryProduct GetById(int id)
        {
            try
            {
                var categoryPro = service.GetById(id);
                if (categoryPro == null)
                {
                    return null;
                }
                else
                {
                    return categoryPro;
                }
            }
            catch (Exception e)
            {
                throw new HttpResponseException(HttpStatusCode.InternalServerError);
            }
        }


        [HttpPut]

        public void Update(CategoryProduct a)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    service.Update(a);
                }
                catch (Exception e)
                {
                    throw new HttpResponseException(HttpStatusCode.InternalServerError);
                }
            }
            else
            {
                throw new HttpResponseException(HttpStatusCode.BadRequest);
            }

        }



        [HttpDelete]

        public void Delete(int id)
        {
            try
            {
                service.Delete(id);
            }
            catch (Exception e)
            {
                throw new HttpResponseException(HttpStatusCode.InternalServerError);
            }
        }


        [HttpPost]

        public int Insert(CategoryProduct a)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    return service.Insert(a);
                }
                catch (Exception e)
                {
                    throw new HttpResponseException(HttpStatusCode.InternalServerError);
                }
            }
            else
            {
                throw new HttpResponseException(HttpStatusCode.BadRequest);
            }
        }

    }
}