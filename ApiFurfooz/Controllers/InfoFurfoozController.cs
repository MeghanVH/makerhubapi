﻿using ApiFurfooz.DAL.Entities;
using ApiFurfooz.DAL.Repositories;
using ApiFurfooz.Models;
using ApiFurfooz.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using HttpDeleteAttribute = System.Web.Http.HttpDeleteAttribute;
using HttpGetAttribute = System.Web.Http.HttpGetAttribute;
using HttpPostAttribute = System.Web.Http.HttpPostAttribute;
using HttpPutAttribute = System.Web.Http.HttpPutAttribute;

namespace ApiFurfooz.Controllers
{
    public class InfoFurfoozController : ApiController
    {

        private InfoFurfoozService service;
        public InfoFurfoozController(InfoFurfoozService service)
        {
            this.service = service;
        }

        [HttpGet]
        public IEnumerable<InfoFurfoozModel> Get()
        {
            try
            {
                IEnumerable<InfoFurfoozModel> infos = service.GetAll();
                return infos;
            }
            catch (Exception e)
            {
                throw new HttpResponseException(HttpStatusCode.InternalServerError);
            }
        }

        [HttpGet]

        public InfoFurfooz GetById(int id)
        {
            try
            {
                var info = service.GetById(id);
                if (info == null)
                {
                    return null;
                }
                else
                {
                    return info;
                }
            }
            catch (Exception e)
            {
                throw new HttpResponseException(HttpStatusCode.InternalServerError);
            }
        }


        [HttpPut]
        public void Update(InfoFurfooz a)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    service.Update(a);
                }
                catch (Exception e)
                {
                    throw new HttpResponseException(HttpStatusCode.InternalServerError);
                }
            }
            else
            {
                throw new HttpResponseException(HttpStatusCode.BadRequest);
            }
        }



        [HttpDelete]
        public void Delete(int id)
        {
            try
            {
                service.Delete(id);
            }
            catch (Exception e)
            {
                throw new HttpResponseException(HttpStatusCode.InternalServerError);
            }
        }


        [HttpPost]
        public int Insert(InfoFurfooz a)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    return service.Insert(a);
                }
                catch (Exception e)
                {
                    throw new HttpResponseException(HttpStatusCode.InternalServerError);
                }
            }
            else
            {
                throw new HttpResponseException(HttpStatusCode.BadRequest);
            }
        }
    }
}