﻿using ApiFurfooz.DAL.Entities;
using ApiFurfooz.Filters;
using ApiFurfooz.Models;
using ApiFurfooz.Services;
using System;
using System.Collections.Generic;
using System.Net;
using System.Web;
using System.Web.Http;
using HttpDeleteAttribute = System.Web.Http.HttpDeleteAttribute;
using HttpGetAttribute = System.Web.Http.HttpGetAttribute;
using HttpPostAttribute = System.Web.Http.HttpPostAttribute;
using HttpPutAttribute = System.Web.Http.HttpPutAttribute;

namespace ApiFurfooz.Controllers
{
    public class BuvetteController : ApiController
    {
        private BuvetteService service;

        public BuvetteController(BuvetteService service)
        {
            this.service = service;
        }

        //strat log ici.  strat != ?.  
        //En core gestion global

        [HttpGet]
        public IEnumerable<BuvetteModel> Get()
        {
            try
            {
                IEnumerable<BuvetteModel> Buvettes = service.GetAll();
                return Buvettes;
            }
            catch(Exception e)
            {
                throw new HttpResponseException(HttpStatusCode.InternalServerError);
            }
        }

        [HttpGet]

        public Buvette GetById(int id)
        {
            try
            {
                var buvette = service.GetById(id);
                if(buvette == null)
                {
                    return null;
                }
                else
                {
                    return buvette;
                }
            }
            catch (Exception e)
            {
                throw new HttpResponseException(HttpStatusCode.InternalServerError);
            }
        }


        [HttpPut]
        //[ApiAuthorization]
        public void Update (Buvette a)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    service.Update(a);
                }
                catch (Exception e)
                {
                    throw new HttpResponseException(HttpStatusCode.InternalServerError);
                }
            }
            else
            {
                throw new HttpResponseException(HttpStatusCode.BadRequest);
            }

        }


        [HttpDelete]
        //[ApiAuthorization]
        public void Delete (int id)
        {
            try
            {
                service.Delete(id);
            }
            catch (Exception e)
            {
                throw new HttpResponseException(HttpStatusCode.InternalServerError);
            }
        }



        [HttpPost]
        //[ApiAuthorization]
        //validation form
        public int Insert (Buvette a)
        {
            //tout dans le try !
            //Verif input ici.
            //produit unique ?

            if (ModelState.IsValid)
            {
                try
                {
                    return service.Insert(a);
                }
                catch (Exception e)
                {
                    throw new HttpResponseException(HttpStatusCode.InternalServerError);
                }
            }
            else
            {
                throw new HttpResponseException(HttpStatusCode.BadRequest);
            }
            
        }

    }
}