﻿using ApiFurfooz.DAL.Entities;
using ApiFurfooz.Models;
using ApiFurfooz.Services;
using System;
using System.Collections.Generic;
using System.Net;
using System.Web.Http;
using HttpDeleteAttribute = System.Web.Http.HttpDeleteAttribute;
using HttpGetAttribute = System.Web.Http.HttpGetAttribute;
using HttpPostAttribute = System.Web.Http.HttpPostAttribute;
using HttpPutAttribute = System.Web.Http.HttpPutAttribute;

namespace ApiFurfooz.Controllers
{
    public class PointInteretController : ApiController
    {

        private PointInteretService service;
        public PointInteretController(PointInteretService service)
        {
            this.service = service;
        }

        [HttpGet]
        public IEnumerable<PointInteretModel> Get()
        {
            try
            {
                IEnumerable<PointInteretModel> PointsOfInterest = service.GetAll();
                return PointsOfInterest;
            }
            catch (Exception e)
            {
                throw new HttpResponseException(HttpStatusCode.InternalServerError);
            }
        }

        [HttpGet]

        public PointInteret GetById(int id)
        {
            try
            {
                var pi = service.GetById(id);
                if (pi == null)
                {
                    return null;
                }
                else
                {
                    return pi;
                }
            }
            catch (Exception e)
            {
                throw new HttpResponseException(HttpStatusCode.InternalServerError);
            }
        }


        [HttpPut]
        public void Update(PointInteret p)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    service.Update(p);
                }
                catch (Exception e)
                {
                    throw new HttpResponseException(HttpStatusCode.InternalServerError);
                }
            }
            else
            {
                throw new HttpResponseException(HttpStatusCode.BadRequest);
            }
        }


        [HttpDelete]
        public void Delete(int id)
        {
            try
            {
                service.Delete(id);
            }
            catch (Exception e)
            {
                throw new HttpResponseException(HttpStatusCode.InternalServerError);
            }
        }


        [HttpPost]
        public string Insert(PointInteret p)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    return service.Insert(p).ToString();
                }
                catch (Exception e)
                {
                    throw new HttpResponseException(HttpStatusCode.InternalServerError);
                }
            }
            else
            {
                throw new HttpResponseException(HttpStatusCode.BadRequest);
            }
        }
    }
}